terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

module "kubernetes_masters" {
  source             = "git::https://gitlab.com/bennyh17/terraform-libvirt-module.git"
  vm_hostname_prefix = "MC-MN-"
  autostart          = true
  vm_count           = 1
  memory             = "4096"
  vcpu               = 2
  system_volume      = 120
  bridge             = "br0"
  dhcp               = true
  os_img_url         = "${var.img_path}"
}

module "kubernetes_workers" {
  source             = "git::https://gitlab.com/bennyh17/terraform-libvirt-module.git"
  vm_hostname_prefix = "MC-WN-"
  autostart          = true
  vm_count           = 2
  memory             = "4096"
  vcpu               = 2
  system_volume      = 200
  bridge             = "br0"
  dhcp               = true
  os_img_url         = "${var.img_path}"
}

output "kubernetes_masters" {
  value = module.kubernetes_masters
}

output "kubernetes_workers" {
  value = module.kubernetes_workers
}
