variable "img_path" {
  description = "Path to existing base QEMU img file"
  type        = string
  default     = "<nix_store_qcow2_image_path>"
}
