variable "img_path" {
  description = "Path to existing base QEMU img file"
  type        = string
  default     = "/nix/store/qdf0sszgxb90bikl5fhxdcpgby3d9klx-nixos-disk-image/nixos.qcow2"
}
