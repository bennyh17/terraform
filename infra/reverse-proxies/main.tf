terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

module "reverse_proxies" {
  source             = "git::https://gitlab.com/bennyh17/terraform-libvirt-module.git"
  vm_hostname_prefix = "MC-RP-"
  autostart          = true
  vm_count           = 1
  memory             = "4096"
  vcpu               = 2
  system_volume      = 300
  bridge             = "br0"
  dhcp               = true
  os_img_url         = "${var.img_path}"
}

output "reverse_proxies" {
  value = module.reverse_proxies
}
