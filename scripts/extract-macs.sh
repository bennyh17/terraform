#!/bin/sh

# virsh list --all --name: This command lists all the domains (virtual machines) managed by libvirt.

# xargs -n1 -I{} sh -c "...": This passes each domain name as an argument to the subsequent command executed in the shell.

# virsh dumpxml {}: This command dumps the XML description of the specified domain.
    
# grep -oP '([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}': This extracts MAC addresses from the XML using a regular expression.
    
# sort -u: This sorts and removes duplicates from the extracted MAC addresses.
    
# sed -e 's/^/MAC address of {} : /': This prefixes each MAC address with "MAC address of [domain name] : ".

virsh list --all --name | xargs -n1 -I{} sh -c "virsh dumpxml {} | grep -oP '([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}' | sort -u | sed -e 's/^/MAC address of {} : /'"
