# Terraform Infrastructure

### Generate qcow image using configuration.nix (disk size is in MB)
nix run github:nix-community/nixos-generators -- -c configuration.nix -f qcow --disk-size 20000
