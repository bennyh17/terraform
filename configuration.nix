{ config, lib, pkgs, ... }:

{
  boot.loader.grub.enable = true;

  networking.hostName = "nixos";

  services.openssh.enable = true;

  users.users.benny = {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
    initialPassword = "changeme";
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDWh9gq+6RMT87VatRlPM7yNxK2Z3KyMfjzyizaOy010 benny@MC-HV-01"
    ];
  };

  environment.systemPackages = with pkgs; [
    vim
    python3
    git
  ];

  system.stateVersion = "23.11";
}
